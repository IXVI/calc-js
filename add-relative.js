var h_rack = 0; //This var makes the first one rack to disappear
var vl_rack = 0; //This probably too
var vr_rack = 0; //This probably too
var left_joint = 0;
var right_joint = 0;

function add_h_rack () {
	var div = document.createElement('div');
	var img = document.createElement('img');

		div.id = 'h-rack'+window.h_rack;
		div.style.width = "98px";
		div.style.height = "48px";
		div.style.backgroundColor = "lightblue";
		div.style.border = "1px dashed gray";
		div.style.display = "inline-block";

		img.src = "images/h-rack100.png"
		img.style.position = "absolute";
		img.style.top = "-136px";
		img.style.width = "112px";

	
	document.getElementById("main-field").appendChild(div);
	document.getElementById("h-rack"+window.h_rack).appendChild(img);
	return window.h_rack++;
};

function remove_h_rack () {
	if (h_rack <= 0) {return h_rack} else {--window.h_rack;};
	var h_rack_id = "h-rack"+window.h_rack;
	var div = document.getElementById(h_rack_id);
	document.getElementById(h_rack_id).parentNode.removeChild(div);
	document.getElementById("h-add").disabled = false; 
	if (window.h_rack == 0) window.left_joint = 0;
	window.right_joint = 0;
};

function add_left_joint () {

	var div = document.createElement('div');

		div.id = 'vl-rack'+window.vl_rack;
		div.style.width = "48px";
		div.style.height = "48px";
		div.style.backgroundColor = "red";
		div.style.border = "1px dashed gray";
		div.style.position = "relative"
//		div.style.display = "block"
		div.style.float = "left";
		div.style.left = "-50px";
	
	if (window.left_joint == 0) {document.getElementById("h-rack0").appendChild(div); window.vl_rack++;};
	window.left_joint = 1;
}

function add_vl_rack () {
	var div = document.createElement('div');

		div.id = 'vl-rack'+window.vl_rack;
		div.style.width = "48px";
		div.style.height = "98px";
		div.style.backgroundColor = "lightpink";
		div.style.border = "1px dashed gray";
		div.style.position = "relative";
//		div.style.display = "block"
		div.style.float = "left";
		div.style.left = "-50px";

	document.getElementById("h-rack0").appendChild(div);
	return window.vl_rack++;
};

function remove_vl_rack () {
	if (vl_rack <= 0) {return vl_rack} else {--window.vl_rack;};
	var vl_rack_id = "vl-rack"+window.vl_rack;
	var div = document.getElementById(vl_rack_id);
	document.getElementById(vl_rack_id).parentNode.removeChild(div);
	if (window.vl_rack == 0) window.left_joint = 0;
};

function add_right_joint () {
	document.getElementById("h-add").disabled = true;

	var current_h_rack = window.h_rack - 1;
	var last_h_rack = "h-rack"+current_h_rack;

	var div = document.createElement('div');

		div.id = 'vr-rack'+window.vr_rack;
		div.style.width = "48px";
		div.style.height = "48px";
		div.style.backgroundColor = "green";
		div.style.border = "1px dashed gray";
		div.style.position = "relative";
	//	div.style.display = "inline-block";
	//	div.style.float = "right";
		div.style.right = "-98px";
	
	if (window.right_joint == 0) {document.getElementById(last_h_rack).appendChild(div); window.vr_rack++;};
	window.right_joint = 1;
}

function add_vr_rack () {
	var current_h_rack = window.h_rack - 1;
	var last_h_rack = "h-rack"+current_h_rack;

	var div = document.createElement('div');

		div.id = 'vr-rack'+window.vr_rack;
		div.style.width = "48px";
		div.style.height = "98px";
		div.style.backgroundColor = "lightgreen";
		div.style.border = "1px dashed gray";
		div.style.position = "relative";
	//	div.style.display = "inline-block";
	//	div.style.float = "right";
		div.style.right = "-98px";

	document.getElementById(last_h_rack).appendChild(div);
	return window.vr_rack++;
};

function remove_vr_rack () {
	if (vr_rack <= 0) {return vr_rack} else {--window.vr_rack;};
	var vr_rack_id = "vr-rack"+window.vr_rack;
	var div = document.getElementById(vr_rack_id);
	document.getElementById(vr_rack_id).parentNode.removeChild(div);
	if (window.vr_rack == 0) {document.getElementById("h-add").disabled = false; window.right_joint = 0;}
};
