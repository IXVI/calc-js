/*
	***TODO***

	Make the width of left and right vertical racks equals to clientWidth to make it dynamic changeable.
	Right now this is predefined values. - IXVI, 06.06.2018 ~Done

	Make the options for the racks, besides the dimension. - IXVI 08.06.2018	

	***

*/
	//	WINDOW VARIABLES	//

var h_rack = 0; //This var makes the first one rack to disappear
var vl_rack = 0; //This probably too
var vr_rack = 0; //This probably too
var left_joint = 0;
var right_joint = 0;
var h_x = 0;
var vl_y = 0;
var vr_y = 0;




	//	HANDLING THE MAIN SECTION	//

function add_h_rack () 
	{
	var div = document.createElement('div');
	var rand1 = 0 + Math.random() * (255 - 0 + 1); //random from 0 to 255
	var rand2 = 0 + Math.random() * (255 - 0 + 1); //random from 0 to 255
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section
	var shelf_count = document.getElementById('shelf-count').value // How many shelfs?

		div.id = 'h-rack'+window.h_rack;
		div.className				= 'h-racks';
		div.style.width 			= h_width/10+"px";
		div.style.height 			= h_depth/10+"px";
		div.style.backgroundColor 	= "rgb(255,"+rand1+","+rand2+")";
		div.style.position 			= "absolute";
		div.style.top 				= window.h_y+"px";
		div.style.left 				= window.h_x+"px";

	document.getElementById("main-field").appendChild(div);

//	THIS PART WILL ADD IMAGE OF RACK

/*
	var img = document.createElement('img');
		img.src = "images/h-rack"+shelf_count+"-100x183.png"
		img.style.position = "absolute";
		img.style.top = "-136px";
		img.style.height = "183px";
		img.style.width = h_width/10+7+"px";
	document.getElementById("h-rack"+window.h_rack).appendChild(img);
*/

	window.h_x += h_width/10; // Counting X-Position for next Horizontal Rack
	window.h_rack++; //Iterate ID of Horizontal Rack (h_rack)
//	document.getElementById("h-depth").disabled = true;	// Make a fixed depth for a Horizontal Rack line.

	for (i=0; i < window.vr_rack; i++)
	{
	document.getElementById("vr-rack"+i).style.right = -h_depth/10-window.h_x+"px";
	}

	}

function remove_h_rack () 
	{
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	if (h_rack <= 0) {removeElementsByClass('vr-racks'); window.right_joint = 0; window.vr_y = 0; window.vr_rack = 0; window.vl_rack = 0; return h_rack} else {--window.h_rack;} // ID counter must not get sub-zero
	var h_rack_id = "h-rack"+window.h_rack;	// find out current Horizontal Rack ID
	var div = document.getElementById(h_rack_id); // point at current Horizontal Rack
	var h_x_minus = document.getElementById('h-rack'+window.h_rack).clientWidth; // Counting X-Position that must be reducted
	document.getElementById(h_rack_id).parentNode.removeChild(div); // Remove itself by its parent
	document.getElementById("h-add").disabled = false; // Set the ability to add a Horizontal Rack to true 
	if (window.h_rack === 0) {window.left_joint = 0} // Set ability to add Left joint if all Horizontal Racks was removed
	if (window.h_rack === 0) {document.getElementById("h-depth").disabled = false} // Set ability to change Depth of rack for Horizontal Line
	window.h_x -= h_x_minus; // Setting the X-Position pointer backwards
	if (window.h_x <= 0) {window.vl_y = 0} // Resetting the Y-Position of Left Added Racks if all Horizontal Racks was removed

	for (i=0; i < window.vr_rack; i++)
	{
	document.getElementById("vr-rack"+i).style.right = -h_depth/10-window.h_x+"px";
	}

	}



	//	HANDLING THE LEFT RACKS	//

function add_left_joint () 
	{
	var div = document.createElement('div');
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section

		div.id 						= 'vl-rack'+window.vl_rack;
		div.className				= 'vl-racks';
		div.style.width 			= h_depth/10+"px"; // Joint must be the same depth as the Main Horizontal Section
		div.style.height 			= h_depth/10+"px"; // Joint is square-formed
		div.style.backgroundColor 	= "red";
		div.style.border 			= "1px dashed gray";
		div.style.position 			= "absolute";
		div.style.left 				= -h_depth/10+"px"; // Joint must be positioned right to it`s depth
		div.style.top 				= window.vl_y+"px";

	if (window.left_joint === 0) {document.getElementById("h-rack0").appendChild(div); window.vl_rack++; window.vl_y += h_depth/10;}
	window.left_joint = 1;
	}

function add_vl_rack () 
	{
	var div = document.createElement('div');
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section

		div.id 						= 'vl-rack'+window.vl_rack;
		div.className				= 'vl-racks';
		div.style.width 			= h_depth/10+"px"; // Vertical Left Rack must be the same depth as the Main Horizontal Section
		div.style.height 			= h_width/10+"px"; // Width of Horizontal Rack becoming Height of Vertical div (angle for 90 deg)
		div.style.backgroundColor 	= "lightpink";
		div.style.border 			= "1px dashed gray";
		div.style.position 			= "absolute";
		div.style.left 				= -h_depth/10+"px"; // Vertical Left Rack must be positioned right to it`s depth
		div.style.top 				= window.vl_y+"px";

	document.getElementById("h-rack0").appendChild(div);
/*
	var img = document.createElement('img');
		img.src = "images/vl-rack100.png"
		img.style.position = "absolute";
	document.getElementById("vl-rack"+window.vl_rack).appendChild(img);
*/
	window.vl_y += h_width/10;
	window.vl_rack++;
	}

function remove_vl_rack () 
	{
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section
	if (vl_rack <= 0) {return vl_rack} else {--window.vl_rack;}
	var vl_rack_id = "vl-rack"+window.vl_rack;
	var div = document.getElementById(vl_rack_id);
	document.getElementById(vl_rack_id).parentNode.removeChild(div);
	window.vl_y -= h_width/10;
	if (window.vl_rack === 0) window.left_joint = 0;
	if (window.vl_y <= 0) window.vl_y = 0;
	};



	//	HANDLING THE RIGHT RACKS	//

function add_right_joint () 
	{
//	document.getElementById("h-add").disabled = true;

//	var current_h_rack = window.h_rack - 1;

	var div = document.createElement('div');
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section

		div.id 						= 'vr-rack'+window.vr_rack;
		div.className				= 'vr-racks';
		div.style.width 			= h_depth/10+"px"; // Vertical Right Joint must be the same depth as the Main Horizontal Section
		div.style.height 			= h_depth/10+"px"; // Joint is square-formed
		div.style.backgroundColor	= "green";
		div.style.border 			= "1px dashed gray";
		div.style.position 			= "absolute";
		div.style.right 			= -h_depth/10-window.h_x+"px"; // Vertical Right Rack must be positioned right to it`s depth
		div.style.top 				= window.vr_y+"px";
	
	if (window.right_joint === 0) {document.getElementById("main-field").appendChild(div); window.vr_rack++; window.vr_y += h_depth/10;}
	window.right_joint = 1;
	};

function add_vr_rack () 
	{
//	var current_h_rack = window.h_rack - 1;

	var div = document.createElement('div');
	var h_depth = document.getElementById('h-depth').value; // Depth of Horizontal Rack Section
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section

		div.id 						= 'vr-rack'+window.vr_rack;
		div.className				= 'vr-racks';
		div.style.width 			= h_depth/10+"px"; // Vertical Right Rack must be the same depth as the Main Horizontal Section
		div.style.height 			= h_width/10+"px"; // Width of Horizontal Rack becoming Height of Vertical div (angle for 90 deg)
		div.style.backgroundColor 	= "lightgreen";
		div.style.border 			= "1px dashed gray";
		div.style.position 			= "absolute";
		div.style.right 			= -h_depth/10-window.h_x+"px"; // Vertical Left Rack must be positioned right to it`s depth
		div.style.top 				= window.vr_y+"px";

	document.getElementById("main-field").appendChild(div);
	window.vr_y += h_width/10;
	window.vr_rack++;
	};

function remove_vr_rack () 
	{
	var h_width = document.getElementById('h-width').value; // Width of Horizontal Rack Section
	if (vr_rack <= 0) {return vr_rack} else {--window.vr_rack;}
	var vr_rack_id = "vr-rack"+window.vr_rack;
	var div = document.getElementById(vr_rack_id);
	document.getElementById(vr_rack_id).parentNode.removeChild(div);
	window.vr_y -= h_width/10;
	if (window.vr_y <= 0) {window.vr_y = 0; document.getElementById("h-add").disabled = false; window.right_joint = 0;}
	};



	//	MAKING INPUT RANGE HAVE CUSTOM STEP	//

function toggleSteps(element)
	{
	var hwstep = parseInt(element.value);
	if (hwstep <= 1000) {element.step = 300;} else {element.step = 200};
	if (hwstep > 1200) {element.step = 300;} else {element.step = 200};
	};


	// Div remover

function removeElementsByClass(className)
{
	var elements = document.getElementsByClassName(className);
	while(elements.length > 0)
	{
		elements[0].parentNode.removeChild(elements[0]);	
	}	
};



